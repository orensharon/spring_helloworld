package com.sharon.spring.hello.world;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by orensharon on 2/27/17.
 */
public class MainApp {
    public static void main(String[] args) {

        // Using AbstractApplicationContext insead of ApplicationContext
        // to ensures a graceful shutdown and calls the relevant destroy methods
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        context.registerShutdownHook();

        HelloIndia objB = (HelloIndia) context.getBean("helloIndia");
        objB.getMessage1();
        objB.getMessage2();
        objB.getMessage3();    }
}
