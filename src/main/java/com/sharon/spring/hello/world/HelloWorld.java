package com.sharon.spring.hello.world;

/**
 * Created by orensharon on 2/27/17.
 */

public class HelloWorld {
    private String message1;
    private String message2;

    public void setMessage1(String message){
        this.message1  = message;
    }

    public void setMessage2(String message){
        this.message2  = message;
    }

    public void getMessage1(){
        System.out.println("World Message1 : " + message1);
    }

    public void getMessage2(){
        System.out.println("World Message2 : " + message2);
    }

    public void setup() {
        System.out.println("Bean " + HelloWorld.class.getSimpleName() + " has constructed");
    }

    public void tearDown() {
        System.out.println("Bean " + HelloWorld.class.getSimpleName() + " has deconstructed");
    }
}
